var x = 10
var y = 25;
var answer = x + y;
document.getElementById("var").innerHTML="Value of x + y is " + answer;
console.log('answer = 35 is generated from js')

const array1 = ["index0", "index1", "index2", "index3"];
var count=3
document.getElementById("stack").innerHTML = array1;
function myFunction1() {
    count = count-1;
  array1.pop();
  document.getElementById("stack").innerHTML = array1;
}

function myFunction2() {
  count = count+1;
  array1.push("new"+count);
  document.getElementById("stack").innerHTML = array1;
}

const array2 = ["index0", "index1", "index2", "index3"];
var count1=3;
document.getElementById("queue").innerHTML = array2;
function myFunction3() {
    count1 = count1-1;
    document.getElementById("queue").innerHTML = "removed - "+ array2.shift()+"<br>"+ array2;
}
function myFunction4() {
    count1+=1;
    document.getElementById("queue").innerHTML = "Array length = "+array2.unshift("new")+"<br>"+array2;
}

document.getElementById("myBtn").addEventListener("click", myFunction5);

function myFunction5() {
  alert ("Hello World!");
}

document.getElementById("imgBtn").addEventListener("click", myFunction6);

function myFunction6() {
    this.style.borderRadius = "50%";
}

function flowers(name, color, season) {
    this.flowerName = name;
    this.flowerColor = color;
    this.bloomingSeason = season;
    }
    
    const roses = new flowers("rose", "Red", "Spring");
    const violets = new flowers("violets", "Blue", "Winter");
    
    document.getElementById("prototype").innerHTML =
    "Roses are " + roses.flowerColor + ". Violets are " + violets.flowerColor; 

 function loadData() {
    var mybtn = document.getElementById("mybtn");
    mybtn.remove();
    var textBox = document.getElementById("myInput");
    textBox.style.display = "block" 
    fetch("https://jsonplaceholder.typicode.com/users")
    .then(result =>{
            return result.json();
        })
        .then(json => showTable(json))

        let row = document.createElement("tr");
        let u_id = document.createElement('th');
        let u_name = document.createElement('th');
        let u_username = document.createElement('th');
        let u_email = document.createElement('th');
        let u_address = document.createElement('th');
        let u_phone = document.createElement('th');
        let u_website = document.createElement('th');
        let u_company = document.createElement('th');


        u_id.innerHTML = "Id"
        u_name.innerHTML = "Name"
        u_username.innerHTML = "Username"
        u_email.innerHTML = "Email"
        u_address.innerHTML = "Address"
        u_phone.innerHTML = "Phone"
        u_website.innerHTML = "Website"
        u_company.innerHTML = "Company"

        row.appendChild(u_id)
        row.appendChild(u_name)
        row.appendChild(u_username)
        row.appendChild(u_email)
        row.appendChild(u_address)
        row.appendChild(u_phone)
        row.appendChild(u_website)
        row.appendChild(u_company)

        row.style.backgroundColor = "gray"

        table.appendChild(row)

 }
function showTable(people) {
    let table = document.getElementById("table");
    for (let i=0; i<(people.length); i++) {
        let user = people[i]; 
        // console.log(user);
        let row = document.createElement("tr");
        let u_id = document.createElement('td');
        let u_name = document.createElement('td');
        let u_username = document.createElement('td');
        let u_email = document.createElement('td');
        let u_address = document.createElement('td');
        let u_phone = document.createElement('td');
        let u_website = document.createElement('td');
        let u_company = document.createElement('td');

        var addressString = user.address.city +", "+ user.address.street+", "+ user.address.suite+", "+ user.address.zipcode;
        var company = user.company.name+", "+user.company.catchPhrase+", "+user.company.bs;

        u_id.innerHTML = user.id
        u_name.innerHTML = user.name
        u_username.innerHTML = user.username
        u_email.innerHTML = user.email
        u_address.innerHTML = addressString
        u_phone.innerHTML = user.phone
        u_website.innerHTML = user.website
        u_company.innerHTML = company

        row.appendChild(u_id)
        row.appendChild(u_name)
        row.appendChild(u_username)
        row.appendChild(u_email)
        row.appendChild(u_address)
        row.appendChild(u_phone)
        row.appendChild(u_website)
        row.appendChild(u_company)

        table.appendChild(row) 
    }
    
}

function mySearch() {

    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");
  
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[1];
      if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }
    }
  }

